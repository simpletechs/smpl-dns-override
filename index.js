const dns = require("dns");
const debug = require('debug')
const log = debug('smpl-dns-override')

const useOverride = typeof process.env.SMPL_USE_DNS_OVERRIDE === 'string' ? process.env.SMPL_USE_DNS_OVERRIDE === '1': (process.env.NODE_ENV == "production" || typeof process.env.SMPL_ENV == "string");

const OVERRIDE_HOST = process.env.SMPL_INTERNAL_LOAD_BALANCER || "proxy-internal";

if (useOverride) {
  if (global._SMPL_DNS_OVERRIDDEN) {
    log(
      "smpl dns overriden is getting initialized twice. Ignoring second time"
    );
    return;
  }
  global._SMPL_DNS_OVERRIDDEN = true;

  // detect $anything+\$SMPL_ENV.smpl\.services
  // this assumes that inter stack/customer communication is not allowed, use service links if necessary
  // must be broader than using process.env.SMPL_DOMAIN because of arthousecnma and filmtastic
  const env = process.env.SMPL_ENV == 'production' ? `(eu0|${process.env.SMPL_ENV})` : process.env.SMPL_ENV;
  const OVERRIDE_REGEX = new RegExp(
    process.env.SMPL_DNS_OVERRIDE_REGEX || `\\.${env || '[\\w-]+'}\\.smpl\\.services$`,
    "i"
  );
  console.log(
    `Routing all internal SMPL traffic to host: ${OVERRIDE_HOST}`,
    OVERRIDE_REGEX
  );

  function override(method) {
    const originalResolver = dns[method];

    dns[method] = function resolveDNSOverride(host, ...args) {
      if (OVERRIDE_REGEX.test(host)) {
          log('[%s]rerouting "%s" to "%s"', method, host, OVERRIDE_HOST)
        return originalResolver(OVERRIDE_HOST, ...args);
      }
      return originalResolver(host, ...args);
    };
  }
  override("lookup"); // uses OS, can block other lookup because of a limited number of threads
  override("resolve");
  override("resolve6");
} else {
  console.log("skipping SMPL DNS override for development");
}
