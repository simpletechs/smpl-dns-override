# smpl-dns-override

reroutes all DNS requests from `$service.$customer.$env.smpl.services` to an internal load balancer by manipulating the Node.js DNS resolver.